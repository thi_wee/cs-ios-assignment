//
//  UIImageView+Extensions.swift
//  CS_iOS_Assignment
//
//  Created by LV on 9/30/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<NSString, UIImage>()
extension UIImageView {
    func loadImageUsingCache(withUrl urlString : String) {
        let url = URL(string: urlString)
        if url == nil {return}
        self.image = nil
        
        let strUniqueIdentifier_Initial = url!.absoluteString
        self.accessibilityLabel = strUniqueIdentifier_Initial
        
        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString)  {
            self.image = cachedImage
            return
        }
        
        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            
            if let image = UIImage(data: data!) {
                let resizeImage = image.resizeImageWith(newSize: CGSize(width: 100, height: 75))
                imageCache.setObject(resizeImage, forKey: urlString as NSString)
                
                let strUniqueIdentifier_Current = self.accessibilityLabel
                if strUniqueIdentifier_Initial != strUniqueIdentifier_Current {
                    //previous download task so ignore
                    return
                }
                DispatchQueue.main.async {
                    self.image = resizeImage
                }
            }
        }).resume()
    }
}
