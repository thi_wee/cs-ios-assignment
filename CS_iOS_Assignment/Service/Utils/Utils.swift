//
//  Utils.swift
//  CS_iOS_Assignment
//
//  Created by LV on 10/1/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    static let shareInstance: Utils = {
        let instance = Utils()
        
        return instance
    }()
    
    func loadImageUsingCache(withUrl urlString : String, completionHandler: @escaping(UIImage?) -> Void) {
        let url = URL(string: urlString)
        if url == nil {
            completionHandler(nil)
            return
        }
        
        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString)  {
            completionHandler(cachedImage)
            return
        }
        
        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            
            if let image = UIImage(data: data!) {
                let resizeImage = image.resizeImageWith(newSize: CGSize(width: 100, height: 75))
                imageCache.setObject(resizeImage, forKey: urlString as NSString)
                DispatchQueue.main.async {
                    completionHandler(resizeImage)
                }
            }
        }).resume()
    }
    
    func convertDateFormat(inputDate: String) -> String {
        if (inputDate.isEmpty || inputDate.count != 10) {
            return inputDate
        }
         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = "yyyy-MM-dd"

         let oldDate = olDateFormatter.date(from: inputDate)

         let convertDateFormatter = DateFormatter()
         convertDateFormatter.dateFormat = "MMM dd, yyyy"

         return convertDateFormatter.string(from: oldDate!)
    }
    
    func convertTime(minute: Int) -> String {
        if (minute == 0) {
            return ""
        }
        return "\((minute % 3600) / 60)h \((minute % 3600) % 60)m"
    }
}
