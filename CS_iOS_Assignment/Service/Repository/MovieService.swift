//
//  MovieService.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import Foundation

class MovieService {
    
    let movieUrl = "https://api.themoviedb.org/3/movie/now_playing?language=en-US&page=undefined&api_key=55957fcf3ba81b137f8fc01ac5a31fb5"
    let defaultSession = URLSession(configuration: .default)
    
    var dataTask: URLSessionDataTask?
    var errorMessage = ""
        
    private func fetchData(url: URL, completionHandler: @escaping(_ result: Data) -> Void) {
        dataTask = defaultSession.dataTask(with: url) { [weak self] data, response, error in
            defer {
                self?.dataTask = nil
            }
            
            if let error = error {
                self?.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
            } else if let data = data {
                DispatchQueue.main.async {
                    completionHandler(data)
                }
            }
        }
        
        dataTask?.resume()
    }
    
    func fetchMovies(completion: @escaping (Result<[Movie], Error>) -> Void) {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/now_playing?language=en-US&page=undefined&api_key=\(Constants.API_KEY)") else {
            return
        }
        
        self.fetchData(url: url) { (data) in
            var response: [String: Any]?
            
            do {
                response = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                guard let array = response!["results"] as? [Any] else {
                  return
                }
                
                let temp = try JSONSerialization.data(withJSONObject: array, options: .fragmentsAllowed)
                
                let value = try JSONDecoder().decode([Movie].self, from: temp)
                DispatchQueue.main.async {
                    completion(.success(value))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }
    
    func fetchPopulars(pageIndex: Int, completionHandler: @escaping(Result<[Movie], Error>) -> Void) {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=\(Constants.API_KEY)&language=en-US&page=\(pageIndex)") else {
            return
        }
        
        self.fetchData(url: url) { (data) in
            do {
                var response: [String: Any]?
                
                response = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                
                guard let array = response!["results"] as? [Any] else {
                  return
                }
                
                let temp = try JSONSerialization.data(withJSONObject: array, options: .fragmentsAllowed)
                
                let value = try JSONDecoder().decode([Movie].self, from: temp)
                DispatchQueue.main.async {
                    completionHandler(.success(value))
                }
            }catch {
                DispatchQueue.main.async {
                    completionHandler(.failure(error))
                }
            }
        }
    }
    
    func fetchMovieDetails(movieID: String, completionHandler: @escaping(Result<Movie, Error>) -> Void) {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/\(movieID)?api_key=\(Constants.API_KEY)") else {
            return
        }
        
        self.fetchData(url: url) { (data) in
            do {
                var response: [String: Any]?
                
                response = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                
                let temp = try JSONSerialization.data(withJSONObject: response as Any, options: .fragmentsAllowed)
                
                let value = try JSONDecoder().decode(Movie.self, from: temp)
                DispatchQueue.main.async {
                    completionHandler(.success(value))
                }
            }catch {
                DispatchQueue.main.async {
                    completionHandler(.failure(error))
                }
            }
        }
    }
    
}
