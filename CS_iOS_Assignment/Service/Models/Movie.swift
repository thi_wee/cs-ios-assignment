//
//  Movie.swift
//  CS_iOS_Assignment
//
//  Created by LV on 9/30/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct Movie: Codable {
    var ID: Int64
    var Poster: String
    var Title: String
    var Rating: Double
    var Duration: String
    var ReleaseDate: String
    var OverView: String
    var Runtime: Int
    var Genres: [Genre]?
    
    enum MovieKeys: String, CodingKey {
        case ID = "id"
        case Poster = "poster_path"
        case Title = "title"
        case Rating = "vote_average"
        case Duration = "duration"
        case ReleaseDate = "release_date"
        case OverView = "overview"
        case Runtime = "runtime"
        case Genres = "genres"
    }
    
    enum GenresKeys: String, CodingKey {
        case Genres = "genres"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: MovieKeys.self)
        self.ID = try container.decode(Int64.self, forKey: .ID)
        self.Poster = try container.decode(String.self, forKey: .Poster)
        self.Title = try container.decode(String.self, forKey: .Title)
        self.Rating = try container.decode(Double.self, forKey: .Rating)
        self.Duration = try container.decodeIfPresent(String.self, forKey: .Duration) ?? ""
        self.ReleaseDate = try container.decode(String.self, forKey: .ReleaseDate)
        self.OverView = try container.decode(String.self, forKey: .OverView)
        self.Runtime = try container.decodeIfPresent(Int.self, forKey: .Runtime) ?? 0
        self.Genres = try container.decodeIfPresent([Genre].self, forKey: .Genres)
    }
}

struct Genre: Codable {
    var ID: Int
    var Name: String
    
    enum GenreKeys: String, CodingKey {
        case ID = "id"
        case Name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: GenreKeys.self)
        self.ID = try container.decode(Int.self, forKey: .ID)
        self.Name = try container.decode(String.self, forKey: .Name)
    }
}
