//
//  RatingView.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

class RatingView: UIView {
    lazy var lblRating: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.textColor = UIColor.white
        v.font = UIFont.boldSystemFont(ofSize: 15)
        v.backgroundColor = .clear
        return v
    }()
    
    lazy var lblPercent: UILabel = {
        let v = UILabel()
        v.text = "%"
        v.translatesAutoresizingMaskIntoConstraints = false
        v.textColor = UIColor.white
        v.font = UIFont.boldSystemFont(ofSize: 5)
        v.backgroundColor = .clear
        return v
    }()
    
    let circleShapeLayer = CAShapeLayer()
    let shapeLayer = CAShapeLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initUI()
    }
    
    func initUI() {
        drawRectBackground()
        self.addSubview(lblRating)
        self.addSubview(lblPercent)
        NSLayoutConstraint.activate([
            lblRating.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            lblRating.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            lblPercent.topAnchor.constraint(equalTo: lblRating.topAnchor, constant: 3),
            lblPercent.leadingAnchor.constraint(equalTo: lblRating.trailingAnchor, constant: 0)
        ])
    }
    
    func removeShape() {
        circleShapeLayer.removeFromSuperlayer()
        shapeLayer.removeFromSuperlayer()
    }
    
    func drawRating(vote: Double) {
        lblRating.text = "\(Int(vote*10))"
        drawCircleShape()
        let path = UIBezierPath(arcCenter: CGPoint(x: self.bounds.midX, y: self.bounds.midY), radius: self.bounds.width/2 - 5, startAngle: CGFloat(-Double.pi / 2), endAngle: CGFloat((vote * Double.pi * 2)/10 - Double.pi / 2), clockwise: true)

        shapeLayer.path = path.cgPath
        shapeLayer.lineWidth = 5.0
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = .round
        shapeLayer.strokeColor = UIColor(named: "RatingGreen")?.cgColor
        if (vote < 5) {
            shapeLayer.strokeColor = UIColor(named: "RatingYellow")?.cgColor
            circleShapeLayer.strokeColor = UIColor(named: "RatingYellowBg")?.cgColor
        }
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func drawRectBackground() {
        let shapeLayer = CAShapeLayer()
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.bounds.width/2)
        
        shapeLayer.path = path.cgPath
        shapeLayer.lineWidth = 1.0
        shapeLayer.strokeColor = UIColor(named: "RatingColor")?.cgColor
        shapeLayer.fillColor = UIColor(named: "RatingColor")?.cgColor
        self.layer.addSublayer(shapeLayer)
    }
    
    func drawCircleShape() {
        let path = UIBezierPath(arcCenter: CGPoint(x: self.bounds.midX, y: self.bounds.midY), radius: self.bounds.width/2 - 5, startAngle: CGFloat(0), endAngle: CGFloat(Double.pi * 2), clockwise: true)
        
        circleShapeLayer.path = path.cgPath
        circleShapeLayer.lineWidth = 5.0
        circleShapeLayer.strokeColor = UIColor(named: "RatingGreenBg")?.cgColor
        circleShapeLayer.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(circleShapeLayer)
    }
}
