//
//  ViewController.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tbMovies: UITableView!
    let movieVM: MovieViewModel = MovieViewModel()
    var isLoadMore: Bool = false
    var isEndData: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
        self.callAPI()
    }
    
    func callAPI() {
        self.movieVM.getMovies { (results) in
            switch results {
            case .success(_):
                self.tbMovies.reloadData()
            case .failure(_):
                break
            }
        }
        
        self.movieVM.getPopular { (results) in
            switch results {
            case .success(_):
                self.tbMovies.reloadData()
            case .failure(_):
                break
            }
        }
    }
    
    fileprivate func initUI() {
        self.tbMovies.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "MovieCell")
        self.tbMovies.register(UINib(nibName: "CollectionPosterCell", bundle: nil), forCellReuseIdentifier: "CollectionPosterCell")
        self.tbMovies.register(UINib(nibName: "LoadMoreCell", bundle: nil), forCellReuseIdentifier: "LoadMoreCell")
        
        self.tbMovies.delegate = self
        self.tbMovies.dataSource = self
    }
    
    func moveToDetail(movie: Movie) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.movie = movie
        present(vc, animated: true, completion: nil)
    }
}
