//
//  DetailViewController.swift
//  CS_iOS_Assignment
//
//  Created by LV on 10/1/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblMovieName: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var tvOverview: UITextView!
    @IBOutlet var bottomView: UIView!
    var movie: Movie?
    let movieVM: MovieViewModel = MovieViewModel()
    var tagsArray:[String] = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
        self.callAPI()
    }
    
    func initUI() {
        self.imgPoster.layer.borderWidth = 1
        self.imgPoster.layer.borderColor = UIColor(named: "BorderColor")?.cgColor
        self.imgPoster.layer.masksToBounds = true
        
        self.lblMovieName.text = movie?.Title
        self.lblReleaseDate.text = Utils.shareInstance.convertDateFormat(inputDate: movie!.ReleaseDate)
        self.tvOverview.text = movie?.OverView
        self.imgPoster.loadImageUsingCache(withUrl: Constants.IMAGE_URL + movie!.Poster)
    }
    
    func callAPI() {
        self.movieVM.getMovieDetails(movieID: String(movie!.ID), completionHandler: { (results) in
            switch results {
            case .success(let data):
                DispatchQueue.main.async {
                    let time = Utils.shareInstance.convertTime(minute: data.Runtime)
                    self.lblReleaseDate.text = "\(self.lblReleaseDate.text!) - \(time)"
                    
                    
                    for item in data.Genres! {
                        self.tagsArray.append(item.Name)
                    }
                    self.createTagCloud(OnView: self.bottomView, withArray: self.tagsArray as [AnyObject])
                }
                break
            case .failure(_):
                break
            }
        })
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tvOverview.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        if self.tvOverview.contentSize.height < 0.25*UIScreen.main.bounds.height {
            tvOverview.translatesAutoresizingMaskIntoConstraints = true
            tvOverview.sizeToFit()
            tvOverview.isScrollEnabled = false
        }
    }
    
    @IBAction func btnCloseAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func createTagCloud(OnView view: UIView, withArray data:[AnyObject]) {
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 30.0
        var ypos: CGFloat = 0.0
        var tag: Int = 1
        for str in data  {
            let startstring = str as! String
            let width = startstring.widthOfString(usingFont: UIFont(name:"verdana", size: 13.0)!)
            let checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(15)
            if checkWholeWidth > UIScreen.main.bounds.size.width - 45.0 {
                xPos = 30.0
                ypos = ypos + 29.0 + 8.0
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + 17.0 + 20 , height: 29.0))
            bgView.layer.cornerRadius = 5
            bgView.backgroundColor = UIColor.white
            bgView.tag = tag
            
            let textlable = UILabel(frame: CGRect(x: 17.0, y: 0.0, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "verdana", size: 13.0)
            textlable.text = startstring
            textlable.textColor = UIColor.black
            bgView.addSubview(textlable)
            
            xPos = CGFloat(xPos) + CGFloat(width) + CGFloat(17.0) + CGFloat(30)
            view.addSubview(bgView)
            tag = tag  + 1
        }
        
    }
}
