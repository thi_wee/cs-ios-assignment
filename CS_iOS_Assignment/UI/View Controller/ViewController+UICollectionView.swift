//
//  ViewController+UICollectionView.swift
//  CS_iOS_Assignment
//
//  Created by LV on 10/1/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movieVM.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let posterCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PosterCell", for: indexPath) as! PosterCell
        let movie = self.movieVM.movies[indexPath.row]
        posterCell.configure(movie: movie)
        
        return posterCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie = self.movieVM.movies[indexPath.row]
        self.moveToDetail(movie: movie)
    }
}
