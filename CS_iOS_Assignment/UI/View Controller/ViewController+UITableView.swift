//
//  ViewController+UITableView.swift
//  CS_iOS_Assignment
//
//  Created by LV on 10/1/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section) {
        case 0:
            return 160
        default:
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch (section) {
        case 0:
            let tbHeader = UINib(nibName: "HeaderTableView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! HeaderTableView
            tbHeader.lblSession.text = "Playing now"
            return tbHeader
        default:
            let tbHeader = UINib(nibName: "HeaderTableView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! HeaderTableView
            tbHeader.lblSession.text = "Most popular"
            return tbHeader
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return 1
        default:
            if self.movieVM.popular.count > 0 {
                return self.movieVM.popular.count + 1
            } else {
                return self.movieVM.popular.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section) {
        case 0:
            let clPosterCell = tableView.dequeueReusableCell(withIdentifier: "CollectionPosterCell", for: indexPath) as! CollectionPosterCell
            clPosterCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            return clPosterCell
        default:
            if indexPath.row >= self.movieVM.popular.count && !self.isEndData {
                let loadMoreCell = tableView.dequeueReusableCell(withIdentifier: "LoadMoreCell", for: indexPath) as! LoadMoreCell
                return loadMoreCell
            }else {
                let movieCell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as! MovieCell
                
                let movie = self.movieVM.popular[indexPath.row]
                movieCell.configure(movie: movie)
                
                return movieCell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isKind(of: LoadMoreCell.self) {
            let loadMoreCell = cell as! LoadMoreCell
            loadMoreCell.loading.startAnimating()
            
            if !self.isLoadMore && !self.isEndData {
                self.isLoadMore = true
                self.movieVM.getPopular { (results) in
                    switch results {
                    case .success(let data):
                        if data.count == 0 {
                            self.isEndData = true
                        }
                        DispatchQueue.main.async {
                            self.tbMovies.reloadData()
                        }
                        break
                    case .failure(_):
                        break
                    }
                    self.isLoadMore = false
                }
                // Call load more
            }else {
                loadMoreCell.loading.stopAnimating()
                self.isEndData = false
                self.isLoadMore = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isKind(of: LoadMoreCell.self) {
            let loadMoreCell: LoadMoreCell = cell as! LoadMoreCell
            loadMoreCell.loading.stopAnimating()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = self.movieVM.popular[indexPath.row]
        self.moveToDetail(movie: movie)
    }
}


