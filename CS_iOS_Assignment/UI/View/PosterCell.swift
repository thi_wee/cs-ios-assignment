//
//  PosterCell.swift
//  CS_iOS_Assignment
//
//  Created by LV on 9/30/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class PosterCell: UICollectionViewCell {
    @IBOutlet weak var imgPoster: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgPoster.image = nil
    }
    
    func configure(movie: Movie) {
        imgPoster.loadImageUsingCache(withUrl: Constants.IMAGE_URL + movie.Poster)
    }
}
