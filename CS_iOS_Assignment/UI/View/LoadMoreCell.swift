//
//  LoadMoreCell.swift
//  CS_iOS_Assignment
//
//  Created by LV on 10/1/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class LoadMoreCell: UITableViewCell {
    @IBOutlet weak var loading: UIActivityIndicatorView!
}
