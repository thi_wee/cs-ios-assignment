//
//  MovieCell.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

//
// MARK: - Movie Cell
//
class MovieCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: RatingView!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var poster: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.poster.layer.borderWidth = 1
        self.poster.layer.borderColor = UIColor(named: "BorderColor")?.cgColor
        self.poster.layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        poster.image = nil
        rating.removeShape()
    }
    
    func configure(movie: Movie) {
        title.text = movie.Title
        releaseDate.text = Utils.shareInstance.convertDateFormat(inputDate: movie.ReleaseDate)
        rating.drawRating(vote: movie.Rating)
        poster.loadImageUsingCache(withUrl: Constants.IMAGE_URL + movie.Poster)
    }
}
