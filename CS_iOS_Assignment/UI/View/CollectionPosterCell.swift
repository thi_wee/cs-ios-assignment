//
//  CollectionPosterCell.swift
//  CS_iOS_Assignment
//
//  Created by LV on 10/1/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class CollectionPosterCell: UITableViewCell {
    @IBOutlet weak var clPosterView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.clPosterView.register(UINib(nibName: "PosterCell", bundle: nil), forCellWithReuseIdentifier: "PosterCell")
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
        clPosterView.delegate = dataSourceDelegate
        clPosterView.dataSource = dataSourceDelegate
        clPosterView.tag = row
        clPosterView.reloadData()
    }
}
